-- SUMMARY --

The jqueryui formatters module provides a suite of new display 
formatters to apply to your fields.
Three new formatters are provided (Dialog, Accordion and Tabs) 
to 'text', 'text_long' and 'text_with_summary' field types.

-- REQUIREMENTS --

Drupal 7.x

-- INSTALLATION --

* Install as usual.
See https://drupal.org/documentation/install/modules-themes/modules-7 
for further information

-- CONFIGURATION --

* Configure the way you want to show your fields in
admin/structure/types/manage/your-content-type/display
if there is any field of type 'text', 'text_long' 
and 'text_with_summary' you will be able to choose three new options 
in the formatter section (Dialog, Accordion or Tabs)

* When you choose one of the new formatters you will be able to manage some 
settings for display custmization


-- CUSTOMIZATION --


-- TROUBLESHOOTING --


-- FAQ --



-- CONTACT --

Current maintainers:
* Alberto García Lamela (Enxebre) - drupal.org/user/1205082

-- NOTE --

Similar modules and main points:

Field formater conditions - https://drupal.org/project/ffc
Custom formatters - https://drupal.org/project/custom_formatters
